This shows how to run tests on Testery.io using Bitbucket

1. First create a repository on bitbucket
2. Clone the repository
3. Add tests to the repository
4. Add repository to Testery.io
